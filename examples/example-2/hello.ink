-> Main

EXTERNAL walkInForest()
EXTERNAL leave()

=== Main ===

 * [hello?]
 
    HELLO!!!

    ** [what a great conversation!]

        ~ leave()
        -> END
        
    ** [okay, bye!] 
    
         ~ leave()
        -> END
    
    
 * [what's up?]
    
    NOT MUCH!    

    ** [do you want to walk in the forest?]

        ~ walkInForest()
        -> END
        
    ** [okay, bye!] 
    
        ~ leave()
        -> END
    

=== function walkInForest ===
  ~ return true
  
 === function leave ===
  ~ return true
  
  