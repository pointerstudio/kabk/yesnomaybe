VAR name = ""

-> Main

=== Main ===
"Hello {name}" the man said. "What is your name?"
 + [Esmeraldo] "My name is Esmeraldo."
    ~ name = "Esmeraldo"
    -> Main
 + [Claudia] "My name is Claudia."
    ~ name = "Claudia"
    -> Main
 + [Lola] "My name is Lola."
    ~ name = "Lola"
    -> Main
 + [Winfried] "My name is Winfried."
    ~ name = "Winfried"
    -> Main
 * [None of your business] "My name is None of your business."
    ~ name = "None of your business"
    The man said: "Goodye then, {name}."
    -> DONE
-> END


    