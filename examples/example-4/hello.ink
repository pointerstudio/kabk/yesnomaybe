VAR name = ""
VAR music = "1085.mp3"


-> GetUserClick

=== GetUserClick ===
 There is a man sitting. 
 * [START]
    # AUDIOLOOP: ambient_loginScreenIntro.mp3
-> Main

=== Main ===
"Hello {name}" the man said. "What is your name?"
 + [Esmeraldo] "My name is Esmeraldo."
    # AUDIO: effects_50_scn5_btn_2.mp3
    # AUDIOLOOP: ambient_champion_spotlight.mp3
    ~ name = "Esmeraldo"
    -> Main
 + [Claudia] "My name is Claudia."
    # AUDIO: effects_50_scn5_btn_2.mp3
    # AUDIOLOOP: ambient_mundo.mp3
    ~ name = "Claudia"
    -> Main
 + [Lola] "My name is Lola."
    # AUDIO: effects_50_scn5_btn_2.mp3
    # AUDIOLOOP: ambient_arcade_early_game.mp3
    ~ name = "Lola"
    -> Main
 + [Winfried] "My name is Winfried."
    # AUDIO: effects_50_scn5_btn_2.mp3
    # AUDIOLOOP: 1085.mp3
    ~ name = "Winfried"
    -> Main
 * [None of your business] "My name is None of your business."
    # AUDIO: effects_50_scn5_btn_2.mp3
    # AUDIOLOOP: Violet.mp3
    ~ name = "None of your business"
    The man said: "Goodye then, {name}."
    -> DONE
-> END


    