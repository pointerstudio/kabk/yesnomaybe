EXTERNAL showHorse()
EXTERNAL hideHorse()
EXTERNAL rotateHorse()
EXTERNAL stopHorse()

Hello person,
welcome to the endless horse show...
-> SeeHorse

=== SeeHorse ===

- Do you want to see the horse?

 + [Yeah]
    ~ showHorse()
    ->RotateHorse()
 + [Nope]
    ~ hideHorse()
    ->SeeHorse()
    
 ->DONE
    

=== RotateHorse ===
    
- Should the horse rotate?

 + [Absolutely!]
    ~ rotateHorse()
    ->SeeHorse()
 + [Rather not...]
    ~ stopHorse()
    ->SeeHorse()
    
 ->DONE
    
    
    
// dummy functions
// you need one per external function
=== function showHorse() ===
  ~ return true
    
=== function hideHorse() ===
  ~ return true
    
=== function rotateHorse() ===
  ~ return true
    
=== function stopHorse() ===
  ~ return true
  
  
  