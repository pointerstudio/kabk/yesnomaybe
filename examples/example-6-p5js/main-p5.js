let horse;
let doesRotate = false;
let isShowingHorse = false;
let rotateXDeg = 45;
let rotateYDeg = 45;

let rotateXSpeed = 0.01;
let rotateYSpeed = 0.01;

function setup() {
    createCanvas(windowWidth, windowHeight, WEBGL);
    noStroke();
    horse = loadModel('horse.stl', true);
}

function draw() {
    background(0);
    clear();

    ambientLight(0,0,255);

    pointLight(255, 125, 255, 100, 120, 1000);

    //specularMaterial(250);
    //shininess(50);

    if (doesRotate) {
        rotateXDeg += rotateXSpeed;
        rotateYDeg += rotateYSpeed;
    }

    if (isShowingHorse) {
        // okay! let's rotate the horse
        // the rotation is absolute,
        // that means, if we don't change
        // rotateXDeg or rotateYDeg
        // the horse will not be rotating any further
        // a.k.a. rotation is not animated
        rotateX(rotateXDeg);
        rotateY(rotateYDeg);
        // the horse is a bit small
        // let's make it bigger
        scale(2.4);
        model(horse);
    }
}

function rotateHorse() {
    // set the global variable
    doesRotate = true;
    // Math.random() is between 0 and 1
    // but since 1 is superfast, let's slow it down
    // why not
    rotateYSpeed = Math.random() * 0.05;
    // we can also use map
    // to change the range of the speed
    // https://p5js.org/reference/#/p5/map
    rotateXSpeed = map(Math.random(),0,1,-0.05,0.05);
}

function stopHorse() {
    // for stopping we just set
    // doesRotate to false
    doesRotate = false;
}
